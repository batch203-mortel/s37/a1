const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseControllers = require("../controllers/courseControllers");

console.log(courseControllers);

// Route for Creating course
router.post("/", auth.verify, courseControllers.addCourse);

// Router for viewing of all courses for admin
router.get("/allCourses", auth.verify, courseControllers.getAllCourses);

// Router for viewing of all Active courses for anyone

router.get("/activeCourses", courseControllers.getAllActive);

// route for viewing specific course
router.get("/:courseId", courseControllers.getCourse);

// route for updating specific course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// route for archiving specific course (admin)

router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse)

module.exports = router;
