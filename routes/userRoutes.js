const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
console.log(userControllers);
router.post("/checkEmail", userControllers.checkEmailExists);

// Route for User Registration
router.post("/register", userControllers.registerUser);

// user authentication
router.post("/login", userControllers.loginUser)

// retrieve and change password
router.get("/details", auth.verify, userControllers.getProfile);

// Route for user details
router.post("/details/:id", userControllers.updateRole);

router.post("/enroll", auth.verify, userControllers.enroll)

module.exports = router;
