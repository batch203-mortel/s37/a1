const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which makes it difficault to decode the information without the defined secret key.
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Tokens

// Token Creation
/*
	Analogy: 
		Pack the gift provided with a lock, which can only be opened using the secret code as the key.
*/

// The "user" parameter will contain the value of the user upon login.
module.exports.createAccessToken = (user) =>{
	// payload of the JWT
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's "sign method".
	// Syntax:
		// jwt.sign(payload,secretOrPrivateKey, [options/callBack function])

	return jwt.sign(data, secret, {});

}

// Token Verification
/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

// Middleware function
module.exports.verify = (request, response, next) => {

	// The token is retrieved from the request header.
	let token = request.headers.authorization;

	// console.log(token);
	if(token !== undefined){
		// response.send({message: "Token received!"})

		token = token.slice(7, token.length)
		
		console.log(token);

		// Validate the "Token" using the "verify" method to decrypt the token using the secret code.

		// Syntax: jwt.verify(token, secretOrPrivateKey, [options/callBackFunction])

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: "Invalid Token!"})
			}
			// if JWT is valid
			else{
				// Allows the application to proceed to the next middleware function/callback function in the route.
				next();
			}
		})
	}
	else{
		response.send({message: "Auth failed! No Token provided!"})
	}
}

// Token decryption
/*
	-Analogy
		Open the gift and get the content
*/

module.exports.decode = (token) =>{
	if(token !== undefined){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return null;
			}
			else{
				// decode method is used to obtain the information from the JWT
				// Syntax: jwt.decode(token, [options])
				// Returns an object with access to the"payload" property which contains the user information stored when the token is generated.
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}

