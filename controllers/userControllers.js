const User = require("../models/User");
// Import Course
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email exists
/*
			Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
		*/


module.exports.checkEmailExists = (request, response) => {
	return User.find({email: request.body.email})
	.then(result =>
	{console.log(result)

	if(result.length > 0){
		return response.send('Email is already taken.')
	}
	else{
		return response.send(false)
	}
	})
	.catch(error => response.send(error));
}
// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser =(request, response) => {
	console.log(request.body);

	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		// Syntax: brcypt.hashSync(dataTBeEncrypted, salt);
			// salt- salt rounds that the bcrypt algorithm  will run to encrypt the password.
		password: bcrypt.hashSync(request.body.password,10),
		mobileNumber: request.body.mobileNumber
	})
	console.log(newUser);
	return newUser.save()
	.then(user => {
		console.log(user);
		response.send(true)
	})
	.catch(error => {
		console.log(error);
		response.send(false);
	})

}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (request, response)=>{
	return User.findOne({email: request.body.email})
	.then(result => {
		// User does not exists
		if(result == null){
			return response.send({message: "No user found"})
		}
		else{
			// Syntax: compareSync(data, encrypted)
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if(isPasswordCorrect){
				return response.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return response.send({message: "Incorrect password ka!"})
			}
		}
	})
}


// S38 Activity
/*
    1. Create a "/details/:id" route that will accept the user’s Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user:
        - Find the document in the database using the user's ID
        - Reassign the password of the returned document to an empty string ("") / ("*****")
        - Return the result back to the postman
    3. Process a GET request at the /details/:id route using postman to retrieve the details of the user.
    4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
    5. Add the link in Boodle.

*/

/*module.exports.getProfile = (request, response) => {
	let updated = {
		password: ""
	}
	User.findByIdAndUpdate(request.params.id, updated, {new: true})
	.then(updatedPassword => response.send(updatedPassword))
	.catch(error => response.send(error));
	}*/

	module.exports.getProfile = (request, response) => {
		const userData = auth.decode(request.headers.authorization);
		console.log(userData);

		return User.findById(userData.id)
		.then(result =>{
			result.password = "***";
			response.send(result);
		})
	}


module.exports.updateRole = (request, response) => {
	let updated = {
		isAdmin : true
	}

	User.findByIdAndUpdate(request.params.id, updated, {new: true})
	.then(updatedRole => response.send(updatedRole))
	.catch(error => response.send(error));
	}


// Enroll a Course
/*
	Steps:
	// Users
	1. Find the document in the database using the user's ID (token)
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
	//Courses
	1. Find the document in the database using the course ID provided in the request body.
	2. Add the user ID to the course enrollees array.
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the suer because we will need to update  2 separate documents when enrolling a user.
module.exports.enroll = async (request, response) => {
		const userData = auth.decode(request.headers.authorization);
	let courseName = await Course.findById(request.body.courseId)
	.then(result=> result.name);
	
	let data = {
		userId: userData.id,
		email: userData.email,
		courseId: request.body.courseId,
		courseName: courseName
	}
	console.log(data);

	//  a user is updated if we receive a "true" value
	let isUserUpdated = await User.findById(data.userId)
	.then(user => {
	user.enrollments.push({
		courseId: data.courseId,
		courseName: data.courseName
	});
	//  save the updated user information in the database.
	return user.save()
	.then(result => {
		console.log(result);
		return true;
	})
	.catch(error => {
		console.log(error);
		return false;
	})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(data.courseId)
	.then(course => {
		course.enrollees.push({
			userId: data.userId,
			email: data.email
		})
		// Minus the slots available by 1
		course.slots -= 1;

		return course.save()
		.then(result => {
			console.log(result);
			return true
		})
		.catch(error => {console.log(error);
			return false})
	})

	console.log(isUserUpdated);

	// Condition will check if the both user and course document have been updated.
	(isUserUpdated && isCourseUpdated) ? response.send(true) : response.send(false)
	

}