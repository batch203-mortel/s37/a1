const Course = require("../models/Course");
const auth = require("../auth");
const bcrypt = require("bcrypt");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new course to the database
*/

module.exports.addCourse = (request, response) => {
	// Create a variable that will contain and instantiates the Course model
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	if(userData.isAdmin){
			let newCourse = new Course({
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				slots: request.body.slots
			})
			/*console.log(newCourse);
		*/
			newCourse.save()
			.then(course =>{
				console.log(course);
				response.send(true);
			})
			.catch(error => {
				console.log(error);
				response.send(false);
			})
	}
	else{
		response.send("You don't have access to this page.");
	}
	
	
}

// Retrieve all course
/*
	Steps:
	1.Retrieve all the courses(active/inactive) from the database.
	2. Verify the role of the current user(Admin).
*/

module.exports.getAllCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		return Course.find({})
		.then(result => response.send(result));
	}
	else{
		return response.status(401).send("You don't have access to this page!");
	}
}

// Retrieve All Active Courses
/*

	Step:
		1. retrieve all the courses from the database with the property "isActive to true".
*/

module.exports.getAllActive = (request, response) => {
	return Course.find({isActive: true})
	.then(result => response.send(result));
}

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (request, response) => {
	console.log(request.params.courseId);
	return Course.findById(request.params.courseId).then(result => response.send(result))
}

// update a course
/*
Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let updateCourse = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		}

		// Syntax
			//  findByIdAndUpdate(documentID, updatesToBeApplied, {new: true})

			return Course.findByIdAndUpdate(request.params.courseId, updateCourse, {new: true})
			.then(result => {
				console.log(result);
				response.send(result);
			})
			.catch(error => {console.log(error);
				response.send(false)})
	}
	else{
		return response.status(401).send("You don't have access to this page!");
	}
}

// Archive a course
	// Soft delete happens when a course status (isActive) is set to false.

module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	let updateIsActiveField = {
		isActive: request.body.isActive
	}
	if(userData.isAdmin){
		return Course.findByIdAndUpdate(request.params.courseId, updateIsActiveField, {new: true})
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else{
		return response.status(401).send("You don't have access to this page!");
	}
}